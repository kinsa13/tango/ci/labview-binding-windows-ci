diff --git a/CMakeLists.txt b/CMakeLists.txt
new file mode 100644
index 0000000..773d02c
--- /dev/null
+++ b/CMakeLists.txt
@@ -0,0 +1,48 @@
+cmake_minimum_required(VERSION 3.13)
+
+if(NOT CMAKE_BUILD_TYPE)
+    message("No build type specified - default is Release")
+        set(CMAKE_BUILD_TYPE Release)
+endif()
+
+project(labview_binding)
+
+set(CMAKE_CXX_STANDARD 14)
+
+if(NOT TANGO_BASE)
+	message(FATAL_ERROR "TANGO_BASE: Tango base directory undefined")
+endif()
+
+if(NOT CINTOOLS_BASE)
+	message(FATAL_ERROR "CINTOOLS_BASE: Cintools base directory undefined")
+endif()
+
+set(cflags "/Ox /Oy- /MP")
+set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${cflags}")
+set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${cflags}")
+
+set(CMAKE_C_FLAGS_RELWITHDEBINFO     "${CMAKE_C_FLAGS_RELWITHDEBINFO} ${cflags} /Zi")
+set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} ${cflags} /Zi")
+
+# Force static compile
+set(CompilerFlags
+	CMAKE_CXX_FLAGS
+	CMAKE_CXX_FLAGS_DEBUG
+	CMAKE_CXX_FLAGS_RELEASE
+	CMAKE_CXX_FLAGS_MINSIZEREL
+	CMAKE_CXX_FLAGS_RELWITHDEBINFO
+	CMAKE_C_FLAGS
+	CMAKE_C_FLAGS_DEBUG
+	CMAKE_C_FLAGS_RELEASE
+	CMAKE_C_FLAGS_MINSIZEREL
+	CMAKE_C_FLAGS_RELWITHDEBINFO
+)
+
+foreach(CompilerFlag ${CompilerFlags})
+	string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
+	set(${CompilerFlag} "${${CompilerFlag}}" CACHE STRING "msvc compiler flags" FORCE)
+endforeach()
+
+add_library(tango_binding SHARED "")
+
+add_subdirectory(src)
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
new file mode 100644
index 0000000..586658a
--- /dev/null
+++ b/src/CMakeLists.txt
@@ -0,0 +1,86 @@
+target_sources(tango_binding
+	PRIVATE
+		DataAdapter.cpp
+		DeviceGroup.cpp
+		DeviceGroupRepository.cpp
+		Endianness.cpp
+		ErrorStack.cpp
+		EventManager.cpp
+		LvDevice.cpp
+		LvDeviceClass.cpp
+		LvDeviceClassFactory.cpp
+		LvDeviceImpl.cpp
+		LvDeviceProxy.cpp
+		LvDeviceProxyRepository.cpp
+		LvDeviceRepository.cpp
+		LvDeviceServer.cpp
+		ObjectManager.cpp
+		TangoBinding.cpp
+		ThreadSafeDeviceProxy.cpp
+		CommonHeader.h
+		Config.h
+		DataAdapter.h
+		DebugTrace.h
+		DeviceGroup.h
+		DeviceGroupRepository.h
+		Endianness.h
+		ErrorStack.h
+		EventManager.h
+		LvDevice.h
+		LvDeviceAppender.h
+		LvDeviceClass.h
+		LvDeviceClassFactory.h
+		LvDeviceImpl.h
+		LvDeviceProxy.h
+		LvDeviceProxyRepository.h
+		LvDeviceRepository.h
+		LvDeviceServer.h
+		ObjectManager.h
+		TargetPlatform.h
+		ThreadSafeDeviceProxy.h
+		Types.h
+)
+
+add_subdirectory(idl)
+add_subdirectory(xml)
+add_subdirectory(yat4tango)
+add_subdirectory(yat)
+
+set(WIN32_LIBS
+	advapi32
+	comctl32
+	iphlpapi
+	ws2_32
+)
+
+set(TANGO_LIBS
+	COS4
+	labview
+	libtango
+	libzmq-mt-s-4_3_4
+	omniDynamic4
+	omniORB4
+	omnithread
+)
+
+target_include_directories(tango_binding PUBLIC ${CMAKE_CURRENT_LIST_DIR})
+target_include_directories(tango_binding PRIVATE "${TANGO_BASE}/include")
+target_include_directories(tango_binding PRIVATE "${CINTOOLS_BASE}")
+
+target_compile_definitions(tango_binding
+	PRIVATE
+		USE_TANGO_6_OR_SUP
+		_TANGO_HAS_THREAD_SAFE_DEVICE_PROXY_
+		ZMQ_STATIC
+)
+
+target_link_directories(tango_binding
+	PRIVATE "${TANGO_BASE}/lib"
+	PRIVATE "${CINTOOLS_BASE}"
+)
+
+target_link_libraries(tango_binding PRIVATE ${TANGO_LIBS} ${WIN32_LIBS})
+target_link_options(tango_binding PUBLIC /DEBUG /OPT:REF /OPT:ICF /INCREMENTAL:NO /FORCE:MULTIPLE /SAFESEH:NO /IGNORE:4006)
+
+install(TARGETS tango_binding RUNTIME DESTINATION runtime)
+install(FILES $<TARGET_PDB_FILE:tango_binding> DESTINATION runtime OPTIONAL)
diff --git a/src/idl/CMakeLists.txt b/src/idl/CMakeLists.txt
new file mode 100644
index 0000000..e30fb92
--- /dev/null
+++ b/src/idl/CMakeLists.txt
@@ -0,0 +1,5 @@
+target_sources(tango_binding
+	PRIVATE
+		tangoDynSK.cpp
+		tangoSK.cpp
+)
diff --git a/src/xml/CMakeLists.txt b/src/xml/CMakeLists.txt
new file mode 100644
index 0000000..9b09543
--- /dev/null
+++ b/src/xml/CMakeLists.txt
@@ -0,0 +1,7 @@
+target_sources(tango_binding
+	PRIVATE
+		parser.cpp
+		tinyxml2.cpp
+		parser.h
+		tinyxml2.h
+)
diff --git a/src/yat/CMakeLists.txt b/src/yat/CMakeLists.txt
new file mode 100644
index 0000000..294ca71
--- /dev/null
+++ b/src/yat/CMakeLists.txt
@@ -0,0 +1,21 @@
+target_sources(tango_binding
+	PRIVATE
+		Exception.cpp
+		any/Any.h
+		any/GenericContainer.h
+		CommonHeader.h
+		config-linux.h
+		config-macosx.h
+		config-win32.h
+		Exception.h
+		Inline.h
+		LogHelper.h
+		Portability.h
+		TargetPlatform.h
+)
+
+add_subdirectory(file)
+add_subdirectory(memory)
+add_subdirectory(threading)
+add_subdirectory(time)
+add_subdirectory(utils)
diff --git a/src/yat/file/CMakeLists.txt b/src/yat/file/CMakeLists.txt
new file mode 100644
index 0000000..d1e889c
--- /dev/null
+++ b/src/yat/file/CMakeLists.txt
@@ -0,0 +1,6 @@
+target_sources(tango_binding
+	PRIVATE
+		FileName.cpp
+		FileName.h
+		Win32FileImpl.cpp
+)
diff --git a/src/yat/memory/CMakeLists.txt b/src/yat/memory/CMakeLists.txt
new file mode 100644
index 0000000..9a87042
--- /dev/null
+++ b/src/yat/memory/CMakeLists.txt
@@ -0,0 +1,7 @@
+target_sources(tango_binding
+	PRIVATE
+		MemBuf.cpp
+		MemBuf.h
+		SharedPtr.h
+		UniquePtr.h
+)
diff --git a/src/yat/threading/CMakeLists.txt b/src/yat/threading/CMakeLists.txt
new file mode 100644
index 0000000..44292db
--- /dev/null
+++ b/src/yat/threading/CMakeLists.txt
@@ -0,0 +1,21 @@
+target_sources(tango_binding
+	PRIVATE
+		Message.cpp
+		MessageQ.cpp
+		SharedObject.cpp
+		Task.cpp
+		WinNtThreadingImpl.cpp
+		impl/WinNtThreadingImpl.h
+		Condition.h
+		Implementation.h
+		Message.h
+		MessageQ.h
+		Mutex.h
+		Pulser.h
+		Semaphore.h
+		SharedObject.h
+		Task.h
+		Thread.h
+		Threading.h
+		Utilities.h
+)
diff --git a/src/yat/time/CMakeLists.txt b/src/yat/time/CMakeLists.txt
new file mode 100644
index 0000000..1069146
--- /dev/null
+++ b/src/yat/time/CMakeLists.txt
@@ -0,0 +1,6 @@
+target_sources(tango_binding
+	PRIVATE
+		Time.cpp
+		Time.h
+		Timer.h
+)
diff --git a/src/yat/utils/CMakeLists.txt b/src/yat/utils/CMakeLists.txt
new file mode 100644
index 0000000..4e2a9bd
--- /dev/null
+++ b/src/yat/utils/CMakeLists.txt
@@ -0,0 +1,15 @@
+target_sources(tango_binding
+	PRIVATE
+		Logging.cpp
+		String.cpp
+		StringTokenizer.cpp
+		Callback.h
+		Logging.h
+		NonCopyable.h
+		Optional.h
+		ReferenceCounter.h
+		Singleton.h
+		String.h
+		StringTokenizer.h
+		XString.h
+)
diff --git a/src/yat4tango/CMakeLists.txt b/src/yat4tango/CMakeLists.txt
new file mode 100644
index 0000000..ca388d5
--- /dev/null
+++ b/src/yat4tango/CMakeLists.txt
@@ -0,0 +1,22 @@
+target_sources(tango_binding
+	PRIVATE
+		DynamicAttribute.cpp
+		DynamicAttributeManager.cpp
+		DynamicCommand.cpp
+		DynamicCommandManager.cpp
+		DynamicInterfaceManager.cpp
+		InnerAppender.cpp
+		CommonHeader.h
+		DynamicAttribute.h
+		DynamicAttributeManager.h
+		DynamicCommand.h
+		DynamicCommandManager.h
+		DynamicInterfaceManager.h
+		ExceptionHelper.h
+		Export.h
+		InnerAppender.h
+		Logging.h
+		LogHelper.h
+		PropertyHelper.h
+		Types.h
+)
-- 
2.30.2

