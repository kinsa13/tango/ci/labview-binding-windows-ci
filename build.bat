echo on
pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd
echo on

pushd labview-binding
git apply ..\cmake.patch || goto :error
popd

curl -L https://gitlab.com/api/v4/projects/35357105/packages/generic/libtango/%TANGO_VER%/libtango_%TANGO_VER%_%VC_VER%_%ARCH%_static_release.zip -o tango.zip && ^
7z x tango.zip && ^
ren libtango_%TANGO_VER%_%VC_VER%_%ARCH%_static_release tango || goto :error

set BASE=%CD:\=/%

md build
pushd build
cmake -DCMAKE_BUILD_TYPE=Release -G Ninja %BASE%/labview-binding -DTANGO_BASE=%BASE%/tango -DCINTOOLS_BASE=%BASE%/cintools  -DCMAKE_INSTALL_PREFIX=%BASE% && ^
ninja install || goto :error
popd

md deploy
cd runtime
7z a ..\deploy\%DIST_NAME% . || goto :error

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
